const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const config = require('../config/config');

module.exports.login = async (request, response) => {
    const expectedUser = await User.findOne({ email: request.body.email });

    if(expectedUser){
        if(bcrypt.compareSync(request.body.password, expectedUser.password)){
            const token = jwt.sign({
                    email: expectedUser.email,
                    userId: expectedUser._id },
                config.jwt, { expiresIn: 3600 });

            response.status(200).json({
                token: `Bearer ${token}`
            })
        } else {
            response.status(401).json({
                message: 'Неверный пароль.'
            })
        }
    } else {
        response.status(404).json({
            message: 'Данный пользователь не зарегистрирован.'
        })
    }
};

module.exports.register = async (request, response) => {
    const expectedUser = await User.findOne({ email: request.body.email });

    if(expectedUser){
        response.status(409).json({
            message: 'Данный email занят.'
        })
    } else {
        const newUser = new User({
            email: request.body.email,
            password: bcrypt.hashSync(request.body.password, bcrypt.genSaltSync(10))
        });

        try {
            await newUser.save();
            response.status(201).json({
                message: 'Вы успешно зарегистрировались.'
            })
        } catch (e) {
            console.log(e);
        }
    }
};