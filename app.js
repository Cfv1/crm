const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config/config')
const authRoute = require('./routes/authRoute');
const analyticsRoute = require('./routes/analyticsRoute');
const categoryRoute = require('./routes/categoryRoute');
const orderRoute = require('./routes/orderRoute');
const positionRoute = require('./routes/positionRoute');

const app = express();

mongoose.connect(config.mongoURL)
    .then(() => console.log('MongoDB connected'))
    .catch((error) => console.log(error));

app.use(cors());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/auth', authRoute);
app.use('/api/analytics', analyticsRoute);
app.use('/api/category', categoryRoute);
app.use('/api/order', orderRoute);
app.use('/api/position', positionRoute);

module.exports = app;
