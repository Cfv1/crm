const express = require('express');
const categoryController = require('../controllers/categoryController');

const router = express.Router();

router.get('/', categoryController.getAll);
router.get('/:id', categoryController.getById);
router.delete('/:id', categoryController.remove);
router.post('/', categoryController.create);
router.patch('/:id', categoryController.update);

module.exports = router;