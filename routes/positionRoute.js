const express = require('express');
const positionController = require('../controllers/positionController');

const router = express.Router();

router.get('/:categoryId', positionController.getByCategoryId);
router.post('/', positionController.create);
router.patch('/:id', positionController.update);
router.delete('/:id', positionController.delete);

module.exports = router;